package com.lagou.dao;

import com.lagou.domain.Student;
import com.lagou.utils.DruidUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.sql.SQLException;
import java.util.List;

public class StudentDao {
    public boolean findNumber(int number) throws SQLException {
        QueryRunner queryRunner = new QueryRunner(DruidUtils.dataSource);
        String sql = "select * from student where id = ?";
        Student student = queryRunner.query(sql, new BeanHandler<Student>(Student.class), number);
        if (student != null) {
            return true;
        }
        return false;
    }

    public int add(Student student) throws SQLException {
        QueryRunner queryRunner = new QueryRunner(DruidUtils.dataSource);
        String sql = "insert into student (number,name,sex,birthday) values(?,?,?,?)";
        Object[] params = {student.getNumber(), student.getName(), student.getSex(), student.getBirthday()};
        int update = queryRunner.update(sql, params);
        return update;
    }

    public List<Student> findAll() throws SQLException {
        QueryRunner queryRunner = new QueryRunner(DruidUtils.dataSource);
        String sql = "select number,name,sex,birthday from student";
        List<Student> list = queryRunner.query(sql, new BeanListHandler<Student>(Student.class));
        return list;
    }
}
