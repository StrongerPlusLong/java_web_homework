package com.lagou.dao;

import com.lagou.domain.User;
import com.lagou.utils.DruidUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;

import java.sql.SQLException;

public class UserDao {
    public User login(String username, String password) throws SQLException {
        QueryRunner queryRunner = new QueryRunner(DruidUtils.dataSource);
        String sql = "select * from user where username = ? and password = ?";
        User user = queryRunner.query(sql, new BeanHandler<User>(User.class), username, password);
        return user;
    }
}
