package com.lagou.service;


import com.lagou.dao.UserDao;
import com.lagou.domain.User;

import java.sql.SQLException;

public class LoginController {


    public static boolean login(String username, String password) throws SQLException {
        UserDao userDao = new UserDao();
        User user = userDao.login(username, password);
        if (user != null) {
            return true;
        }
        return false;
    }
}
