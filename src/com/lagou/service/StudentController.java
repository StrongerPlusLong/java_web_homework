package com.lagou.service;

import com.lagou.dao.StudentDao;
import com.lagou.domain.Student;


import java.sql.SQLException;
import java.util.List;

public class StudentController {
    public static boolean add(Student student) throws SQLException {
        StudentDao studentDao = new StudentDao();

        // 返回数字不为0,则插入成功
        int number = studentDao.add(student);
        if (number != 0) {
            return true;
        }
        return false;
    }

    public static boolean findNumber(Student student) throws SQLException {
        StudentDao studentDao = new StudentDao();
        // 当查询到number时,证明这个学号已存在
        if (studentDao.findNumber(student.getNumber())) {
            return true;
        }
        return false;
    }

    public static List<Student> findAll() throws SQLException {
        StudentDao studentDao = new StudentDao();
        return studentDao.findAll();
    }

}
