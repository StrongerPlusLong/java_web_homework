package com.lagou.domain;

import java.util.Date;
import java.util.Objects;

public class Student {
    private int number;
    private String name;
    private String sex;
    private String birthday;

    public Student(int number, String name, String sex, String birthday) {
        this.number = number;
        this.name = name;
        this.sex = sex;
        this.birthday = birthday;
    }

    public Student() {
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;
        Student student = (Student) o;
        return number == student.number &&
                name.equals(student.name) &&
                sex.equals(student.sex) &&
                birthday.equals(student.birthday);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, name, sex, birthday);
    }

    @Override
    public String toString() {
        return "Student{" +
                "number=" + number +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", birthday=" + birthday +
                '}';
    }
}
