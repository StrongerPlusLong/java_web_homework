package com.lagou.web;

import com.lagou.domain.Student;
import com.lagou.service.StudentController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AddStudent extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String number = req.getParameter("number");
        String name = req.getParameter("name");
        String sex = req.getParameter("sex");
        String birthday = req.getParameter("birthday");

        resp.setContentType("text/html;charset=utf-8");

        PrintWriter writer = resp.getWriter();

        int studentNumber = Integer.parseInt(number);


        try {
            Student student = new Student(studentNumber, name, sex, birthday);
            if (StudentController.findNumber(student)) {
                writer.write("学号已存在!!!");
            }
            if (StudentController.add(student)) {
                resp.sendRedirect("/index");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
