package com.lagou.web;


import com.lagou.domain.Student;
import com.lagou.service.LoginController;
import com.lagou.service.StudentController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

public class Login extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect("/login.html");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        boolean isLogin = false;
        try {
            isLogin = LoginController.login(username, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (isLogin) {
            HttpSession session = req.getSession();
            session.setAttribute("username", username);

            resp.sendRedirect("/index");

        } else {
            resp.setContentType("text/html;charset=utf-8");

            // 向页面输出中文
            PrintWriter writer = resp.getWriter();
            writer.write("用户名或密码错误");
        }

    }
}
