## 项目启动前准备
### 修改数据配置
在src目录下的druid.properties修改数据库相关配置
### 数据库创建并导入数据
```shell script
CREATE DATABASE homework CHARACTER SET utf8;  # 创建数据库
use homework;   # 切换数据库
source homework.sql文件地址   # 导入数据
```
### 测试账户密码
账号:test

密码:123456
