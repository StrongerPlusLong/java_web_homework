<%@ page import="com.lagou.domain.Student" %>
<%@ page import="java.util.List" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page contentType="text/html; charset=gb2312" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>index</title>
</head>
<body>
<h1>登录成功!!!</h1>
<%--<%--%>
<%--    List<Student> list = (List<Student>) request.getAttribute("studentList");--%>
<%--    for (int i = 0; i < list.size(); i++) {--%>
<%--        System.out.println(list.get(i));--%>
<%--    }--%>
<%--%>--%>

<table>
    <tr>
        <td>学号</td>
        <td>姓名</td>
        <td>性别</td>
        <td>出生日期</td>
    </tr>
    <% List<Student> list = (List<Student>) request.getAttribute("studentList");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        for (int i = 0; i < list.size(); i++) {%>
    <tr>
        <td><%=list.get(i).getNumber()%>
        </td>
        <td><%=list.get(i).getName()%>
        </td>
        <td><%=list.get(i).getSex()%>
        </td>
        <td>
            <%=list.get(i).getBirthday()%>
        </td>
    </tr>
    <%}%>
</table>
<a href="/add.html">添加学生</a>
</body>
</html>